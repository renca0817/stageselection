package com.StageSelection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StageSelectionApplication {

	public static void main(String[] args) {
		SpringApplication.run(StageSelectionApplication.class, args);
	}
}
